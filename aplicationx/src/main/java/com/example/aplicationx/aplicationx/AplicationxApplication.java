package com.example.aplicationx.aplicationx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicationxApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplicationxApplication.class, args);
	}

}
