package com.example.aplicationx.aplicationx.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.aplicationx.aplicationx.entities.Usuario;
import com.example.aplicationx.aplicationx.repositories.UsuarioRepository;

@Service
public class UsuarioServicesImpl implements UsuarioService{

	@Autowired
	private UsuarioRepository repository;
	
	@Override
	public List<Usuario> consultaUsuario() {
		return repository.findAll();
	}
	
	@Override
	public Usuario consultaUsuario(Long usuarioId) {
		Optional<Usuario> usuario = repository.findById(usuarioId);
		if(usuario.isPresent()) {
			return usuario.get();
		}
		return new Usuario();
	}

	@Override
	public void guardaUsuario(Usuario usuario) {
		repository.save(usuario);		
	}

	@Override
	public void borraUsuario(Long usuarioId) {
		repository.deleteById(usuarioId);		
	}

	@Override
	public void actualizaUsuario(Usuario usuario) {
		repository.save(usuario);		
	}
	
}
