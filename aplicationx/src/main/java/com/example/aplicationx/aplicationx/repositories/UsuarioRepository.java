package com.example.aplicationx.aplicationx.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.aplicationx.aplicationx.entities.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
